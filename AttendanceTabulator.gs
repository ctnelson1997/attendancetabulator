
const DEFAULT_DATE_START = "T12:00:00-06:00";
const DEFAULT_DATE_END   = "T18:00:00-06:00";
function constructCode(date) {
  return {dateStart: new Date(date + DEFAULT_DATE_START), dateEnd: new Date(date + DEFAULT_DATE_END)};
}

let codeMap = new Map();
codeMap.set("JWF", constructCode("2021-01-27"));
codeMap.set("DLS", constructCode("2021-01-29"));
codeMap.set("WOT", constructCode("2021-02-01"));
codeMap.set("KWR", constructCode("2021-02-03"));
codeMap.set("WRR", constructCode("2021-02-05"));
codeMap.set("LKW", constructCode("2021-02-08"));
codeMap.set("WEL", constructCode("2021-02-10"));
codeMap.set("VWO", constructCode("2021-02-12"));
codeMap.set("LRE", constructCode("2021-02-15"));
codeMap.set("AWS", constructCode("2021-02-17"));
codeMap.set("WGR", constructCode("2021-02-19"));
codeMap.set("QAL", constructCode("2021-02-22"));
codeMap.set("CWS", constructCode("2021-02-24"));
codeMap.set("LKD", constructCode("2021-02-26"));
codeMap.set("UWL", constructCode("2021-03-01"));
codeMap.set("XCV", constructCode("2021-03-03"));
codeMap.set("PWE", constructCode("2021-03-05"));
codeMap.set("LKF", constructCode("2021-03-08"));
codeMap.set("OLR", constructCode("2021-03-10"));
codeMap.set("YWE", constructCode("2021-03-12"));
codeMap.set("MWR", constructCode("2021-03-15"));
codeMap.set("GEP", constructCode("2021-03-17"));
codeMap.set("WYR", constructCode("2021-03-22"));
codeMap.set("QMA", constructCode("2021-03-26"));
codeMap.set("NBW", constructCode("2021-03-29"));
codeMap.set("WKA", constructCode("2021-03-31"));
codeMap.set("QTL", constructCode("2021-04-05"));
codeMap.set("DXW", constructCode("2021-04-07"));
codeMap.set("NPQ", constructCode("2021-04-09"));
codeMap.set("DFO", constructCode("2021-04-12"));
codeMap.set("YUP", constructCode("2021-04-14"));
codeMap.set("CUA", constructCode("2021-04-19"));
codeMap.set("JWM", constructCode("2021-04-21"));
codeMap.set("WPR", constructCode("2021-04-23"));
codeMap.set("WYX", constructCode("2021-04-26"));
codeMap.set("WHR", constructCode("2021-04-28"));

// Will be populated in the run function.
let studentMap = new Map();

const DATA_RANGE = 'A3:C4885';
function runAttendanceAnalysis() {
  let data = SpreadsheetApp.getActiveSheet().getRange(DATA_RANGE).getValues();
  let numRows = SpreadsheetApp.getActiveSheet().getRange(DATA_RANGE).getNumRows();
  for (let i = 0; i < numRows; i++) {
    let timestamp = data[i][0];
    let email = data[i][1].toLowerCase().trim();
    let typedCode = data[i][2].toUpperCase().trim();

    if(!studentMap.has(email)) {
      studentMap.set(email, {attendance: 0, codesClaimed: []});
    }

    if(codeMap.has(typedCode) &&
        codeMap.get(typedCode).dateStart.valueOf() < timestamp.valueOf() &&
        codeMap.get(typedCode).dateEnd.valueOf() > timestamp.valueOf() &&
        !studentMap.get(email).codesClaimed.includes(typedCode)) {
          let student = studentMap.get(email);
          student.attendance += 1;
          student.codesClaimed.push(typedCode);
    }
  }

  let students = [];
  studentMap.forEach((student, email) => {
    students.push({email: email, attendance: student.attendance});
  });
  students.sort((a, b) => b.attendance - a.attendance);
  Logger.log("Email,% Attendance,# Attendance")
  students.forEach(student => {
    Logger.log(student.email + "," + parseFloat((student.attendance / codeMap.size) * 100).toFixed(1)+"%" + "," + student.attendance);
  });
}
